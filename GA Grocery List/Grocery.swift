//
//  Grocery.swift
//  GA Grocery List
//
//  Created by Arjun Srivastava on 6/4/16.
//  Copyright © 2016 Arjun Srivastava. All rights reserved.
//

import Foundation

class Grocery: NSObject, NSCoding{
    var name: String!
    var details: String!
    var completed: Bool = false
    var quantity: Int = 0
    
    override init() {
        name = ""
        details = ""
    }
    
    init(name: String, details: String, quantity: Int) {
        self.name = name
        self.details = details
        self.quantity = quantity
    }

    required init?(coder aDecoder: NSCoder) {
        super.init()
        
        self.name = aDecoder.decodeObjectForKey("name") as! String
        self.quantity = aDecoder.decodeIntegerForKey("quantity")
        self.details = aDecoder.decodeObjectForKey("details") as! String
        self.completed = aDecoder.decodeBoolForKey("completed")
        
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(self.name, forKey: "name")
        aCoder.encodeObject(self.details, forKey: "details")
        aCoder.encodeBool(self.completed, forKey: "completed")
        aCoder.encodeInteger(self.quantity, forKey: "quantity")
    }
}