//
//  GroceryDetailsTableViewCell.swift
//  GA Grocery List
//
//  Created by Arjun Srivastava on 6/4/16.
//  Copyright © 2016 Arjun Srivastava. All rights reserved.
//

import UIKit

class GroceryDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var detailsTextField: UITextField!
    @IBOutlet weak var quantityStepper: UIStepper!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        layer.borderColor = UIColor.blackColor().CGColor
        layer.borderWidth = 1.0
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
