//
//  GroceryItemTableViewCell.swift
//  GA Grocery List
//
//  Created by Arjun Srivastava on 6/4/16.
//  Copyright © 2016 Arjun Srivastava. All rights reserved.
//

import UIKit

class GroceryItemTableViewCell: UITableViewCell {

    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var checkButton: UIButton!
    @IBOutlet weak var itemNameTextField: UITextField!
    @IBOutlet weak var lineView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        itemNameTextField.enabled = false
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
