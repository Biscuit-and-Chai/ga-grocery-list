//
//  GroceryTableViewController.swift
//  GA Grocery List
//
//  Created by Arjun Srivastava on 6/4/16.
//  Copyright © 2016 Arjun Srivastava. All rights reserved.
//

import UIKit

extension Array { //Create an extension to remove an object from an array

    mutating func removeObject<U: Equatable>(object: U) -> Bool {
        for (index, compareObject) in self.enumerate() {
            if let to = compareObject as? U {
                if object == to {
                    self.removeAtIndex(index)
                    return true
                }
            }
        }
        return false
    }

}

class GroceryTableViewController: UITableViewController, UITextFieldDelegate {

    var groceries: [Grocery] = []
    
    var groceryDetailRow: Int = -1 //The index of the detail cell for a specific grocery item
    
    var addButton: UIBarButtonItem!
    var saveButton: UIBarButtonItem!
    
    var isAdding: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
        let filePath = (documentsPath as NSString).stringByAppendingPathComponent("groceries.plist")
        
        if NSFileManager.defaultManager().fileExistsAtPath(filePath) {
            self.groceries = NSKeyedUnarchiver.unarchiveObjectWithFile(filePath) as! [Grocery]
        } else {
            groceries += [Grocery(name: "Grapes", details: "Seedless", quantity: 2), Grocery(name: "Apples", details: "Red apples", quantity: 4), Grocery(name: "Bananas", details: "Ripe bananas", quantity: 8)]
        }
        
        addButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Add, target: self, action: #selector(self.addGroceryItem(_:)))
        saveButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Save, target: self, action: #selector(self.saveGroceryItem(_:)))
        
        self.navigationItem.setRightBarButtonItem(addButton, animated: false)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groceries.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let grocery = groceries[indexPath.row]

        if indexPath.row == groceryDetailRow { //Single grocery item detail cell
            let cell = tableView.dequeueReusableCellWithIdentifier("detail_cell", forIndexPath: indexPath) as! GroceryDetailsTableViewCell
            
            cell.detailsTextField.tag = 2
            
            cell.detailsTextField?.text = grocery.details

            if grocery.details.isEmpty { //Assumes empty details means new item
                cell.detailsTextField?.enabled = true
                
            } else {
                cell.detailsTextField.enabled = false
            }

            
            if grocery.completed {
                cell.backgroundColor = UIColor.lightGrayColor()
                cell.quantityStepper.hidden = true
                
            } else {
                cell.backgroundColor = UIColor.whiteColor()
                cell.quantityStepper.hidden = false
                cell.quantityStepper.value = Double(grocery.quantity)

            }

            
            return cell

        } else { //Grocery item cell
            let cell = tableView.dequeueReusableCellWithIdentifier("grocery_cell", forIndexPath: indexPath) as! GroceryItemTableViewCell
            
            cell.itemNameTextField.tag = 1
            
            cell.itemNameTextField?.text = grocery.name
            cell.quantityLabel?.text = "\(grocery.quantity)"
            
            cell.checkButton.tag = indexPath.row
            
            if grocery.completed {
                cell.backgroundColor = UIColor.lightGrayColor()
                cell.lineView.hidden = false
                cell.checkButton.setImage(UIImage(named: "checkedbox"), forState: .Normal)
                
            } else {
                cell.backgroundColor = UIColor.whiteColor()
                cell.lineView.hidden = true
                cell.checkButton.setImage(UIImage(named: "uncheckedbox"), forState: .Normal)

            }
            
            if grocery.name.isEmpty { //Assumes empty name means new item
                cell.itemNameTextField.enabled = true
            } else {
                cell.itemNameTextField.enabled = false
            }
            
            return cell
        }

    }

    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row == groceryDetailRow {
            return 114 //Size for grocery detail item cell
        }
        
        return 70 //Size for grocery item cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if isAdding == true {
            return
        }
        
        tableView.beginUpdates()
        
        if groceryDetailRow-1 == indexPath.row {
            //Selected cell that currently is showing details -> hide details
            groceries.removeAtIndex(groceryDetailRow)
            
            tableView.deleteRowsAtIndexPaths([NSIndexPath(forRow: groceryDetailRow, inSection: 0)], withRowAnimation: .Fade)
            
            groceryDetailRow = -1
            
            
        } else if groceryDetailRow != indexPath.row {
            //Selected cell to show details -> hide previous details -> show new cell details
            
            if groceryDetailRow != -1 {
                //Hide previous details
                groceries.removeAtIndex(groceryDetailRow)
            
                tableView.deleteRowsAtIndexPaths([NSIndexPath(forRow: groceryDetailRow, inSection: 0)], withRowAnimation: .Fade)
            }
            
            if groceryDetailRow > indexPath.row || groceryDetailRow == -1 {
                //If the previous selected detail had a higher index than the current selected index, or if there was no previous detail selected
                
                groceryDetailRow = indexPath.row + 1 //Add one to the indexPath to show detail after selected item cell
                
                groceries.insert(groceries[indexPath.row], atIndex: groceryDetailRow)
                
            } else {
                //If the previous selected detail had a lower index than the current selected index, the selected index path will already be incremented by 1 due to the detail cell being shown already
                
                groceryDetailRow = indexPath.row
                
                groceries.insert(groceries[indexPath.row-1], atIndex: groceryDetailRow)
            }
            
            tableView.insertRowsAtIndexPaths([NSIndexPath(forRow: groceryDetailRow, inSection: 0)], withRowAnimation: .Fade)
        }
        
        tableView.endUpdates()
        
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    
        if editingStyle == .Delete { //Swipe to delete
            tableView.beginUpdates()
            
            if groceryDetailRow != -1 {
                let grocery = groceries[groceryDetailRow]
                groceries.removeObject(grocery)
                groceries.removeAtIndex(indexPath.row)

                tableView.deleteRowsAtIndexPaths([indexPath, NSIndexPath(forRow: groceryDetailRow, inSection: 0)], withRowAnimation: .Fade)
            } else {
                groceries.removeAtIndex(indexPath.row)

                tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            }
            
            let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
            let filePath = (documentsPath as NSString).stringByAppendingPathComponent("groceries.plist")
            
            NSKeyedArchiver.archiveRootObject(groceries, toFile: filePath)
            
            tableView.endUpdates()
            
            groceryDetailRow = -1
            tableView.reloadData()
        }
    }
    
    
    //MARK: - Buttons
    
    @IBAction func addGroceryItem(sender: AnyObject) {
        groceries.insert(Grocery(), atIndex: 0) //Insert blank grocery item to top of array/table view
        
        tableView.insertRowsAtIndexPaths([NSIndexPath(forRow: 0, inSection: 0)], withRowAnimation: UITableViewRowAnimation.Fade)
        
        if groceryDetailRow != -1 { //Make sure no grocery details is being shown
            groceries.removeAtIndex(groceryDetailRow)
        }
        groceryDetailRow = 1 //Show details cell for new item for editting
        groceries.insert(groceries[0], atIndex: groceryDetailRow) //Insert copy of new grocery item
        
        tableView.reloadData()
        
        isAdding = true
    
        self.navigationItem.setRightBarButtonItem(saveButton, animated: false)
    }
    
    func saveGroceryItem(sender: AnyObject) {

        let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
        let filePath = (documentsPath as NSString).stringByAppendingPathComponent("groceries.plist")
        
        var saveGroceries = groceries
        saveGroceries.removeAtIndex(groceryDetailRow) //We don't want to save two of the same groceries
        NSKeyedArchiver.archiveRootObject(saveGroceries, toFile: filePath)

        isAdding = false

        self.navigationItem.setRightBarButtonItem(addButton, animated: false)
    }
    
    @IBAction func checkOffItem(sender: AnyObject) {
        let checkbox = sender as! UIButton
        
        let grocery = groceries[checkbox.tag] //checkbox.tag == indexPath.row of selected cell
        
        if grocery.completed == true{
            grocery.completed = false
            checkbox.setImage(UIImage(named: "uncheckedbox"), forState: .Normal)
            
        } else {
            grocery.completed = true
            checkbox.setImage(UIImage(named: "checkedbox"), forState: .Normal)
        }
        
        tableView.reloadData()
    }

    @IBAction func incrementQuantity(sender: AnyObject) {
        let stepper = sender as! UIStepper
        
        //select original grocery item cell
        let grocery = groceries[groceryDetailRow-1]
        
        grocery.quantity = Int((stepper).value)
        
        let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
        let filePath = (documentsPath as NSString).stringByAppendingPathComponent("groceries.plist")
        
        var saveGroceries = groceries
        saveGroceries.removeAtIndex(groceryDetailRow) //We don't want to save two of the same groceries
        NSKeyedArchiver.archiveRootObject(saveGroceries, toFile: filePath)

        tableView.reloadData()
    }
    
    @IBAction func clearCheckedOffItems(sender: AnyObject) {
        for grocery in groceries {
            if grocery.completed == true {
                groceries.removeObject(grocery)
            }
        }
        
        tableView.reloadData()
    }
    
    
    //MARK: - Text Delegates
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        let grocery = groceries[0]

        if textField.tag == 1 {
            grocery.name = textField.text
        } else {
            grocery.details = textField.text
        }
    }
}

